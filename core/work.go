package core

import (
	"errors"
	"fmt"
	"github.com/DamnWidget/goqueue"
	"github.com/brentp/go-chartjs"
	"github.com/brentp/go-chartjs/types"
	"github.com/google/uuid"
	"github.com/wcharczuk/go-chart"
	"log"
	"math/rand"
	"sort"
	"strconv"
	"strings"
	"time"
)

type ClientTypeRegistration struct {
	ID     string `json:"id"`
	Syntax string `json:"syntax"`
}

type Job struct {
	ID         string   `json:"id"`
	ClientType string   `json:"clientType"`
	Title      string   `json:"title"`
	Timeout    int      `json:"timeout"`
	Body       []string `json:"body"`
	Metrics    []string `json:"metrics"`
}

type MetricTime struct {
	Start    time.Time `json:"start"`
	End      time.Time `json:"stop"`
	Duration float64   `json:"duration_s"`
	Comment  string    `json:"comment"`
}

type Metric struct {
	Name       string                `json:"name"`
	Timestamps map[string]MetricTime `json:"times"`
}

type MetricReport struct {
	ID      string `json:"id"`
	Name    string `json:"name"`
	Value   string `json:"value"`
	Comment string `json:"comment"`
}

// satisfy the required interface with this struct and methods.
type xy struct {
	x []float64
	y []float64
	r []float64
}

func (v xy) Xs() []float64 {
	return v.x
}
func (v xy) Ys() []float64 {
	return v.y
}
func (v xy) Rs() []float64 {
	return v.r
}

func Check(e error) {
	if e != nil {
		log.Fatal(e)
	}
}

func NewServer() *ConductorServer {
	return &ConductorServer{
		jobs:        make(map[string]*goqueue.Queue),
		metrics:     make(map[string]Metric),
		clientTypes: make(map[string]string),
	}
}

type ConductorServer struct {
	jobs        map[string]*goqueue.Queue
	metrics     map[string]Metric
	clientTypes map[string]string
}

func fixDataset(points xy) xy {
	var working xy

	x := make(map[float64]float64)
	for _, value := range points.x {
		x[value] = x[value] + 1
	}

	keys := make([]float64, 0, len(x))
	for k := range x {
		keys = append(keys, k)
	}
	sort.Float64s(keys)

	for _, value := range keys {
		working.x = append(working.x, value)
		working.y = append(working.y, x[value])
	}

	return working
}

func (s *ConductorServer) AddClientType(client string, syntax string) {
	s.clientTypes[client] = syntax
}

func (s *ConductorServer) GetClientTypes() map[string]string {
	return s.clientTypes
}

func (s *ConductorServer) GetJobs() map[string][]Job {
	result := make(map[string][]Job)
	var j Job
	for key, value := range s.jobs {
		var array = make([]Job, 0)
		for _, item := range value.Values() {
			j = item.(Job)
			array = append(array, j)
		}
		result[key] = array
	}

	return result
}

func (s *ConductorServer) addMetric(metric string) (error, string, Metric) {
	for _, metricName := range s.metrics {
		if metricName.Name == metric {
			return errors.New("Already exists"), "", Metric{}
		}
	}
	var id = uuid.New().String()
	var created = Metric{metric, map[string]MetricTime{}}
	for i := 0; i < 10; i++ {
		var datauid = uuid.New().String()
		var start = time.Now()
		var end = start.Add(time.Second * time.Duration(rand.Int63n(10)))
		created.Timestamps[datauid] = MetricTime{start, end, end.Sub(start).Seconds(), ""}
	}
	s.metrics[id] = created
	return nil, id, created
}

func (s *ConductorServer) GetMetric(id string) (error, Metric) {
	if _, ok := s.metrics[id]; ok {
		return nil, s.metrics[id]
	} else {
		return fmt.Errorf("metric %s not found", id), Metric{}
	}
}

func (s *ConductorServer) GetMetrics() map[string]Metric {
	return s.metrics
}

func (s *ConductorServer) findMetric(name string) (error, Metric) {
	for _, metric := range s.metrics {
		if metric.Name == name {
			return nil, metric
		}
	}
	return fmt.Errorf("Unable to locate metric %s", name), Metric{}
}

func (s *ConductorServer) AddMetricData(name string, id string, timestamp string, comment string) (error, Metric) {
	if !strings.Contains(name, ":") || !(strings.HasPrefix(name, "E:") || strings.HasPrefix(name, "S:")) {
		return fmt.Errorf("Bad formatted metric name, expected either S:metric or E:metric"), Metric{}
	}

	splitName := strings.Split(name, ":")
	metricBaseName := splitName[2]
	timestampType := splitName[0]
	err, metric := s.findMetric(metricBaseName)
	if err != nil {
		return err, Metric{}
	}

	converted, err := strconv.ParseInt(timestamp, 10, 64)
	if err != nil {
		return fmt.Errorf("Time value (%s) isn't an int64 value", timestamp), Metric{}
	}

	var editMetric MetricTime

	if _, ok := metric.Timestamps[id]; !ok {
		// Doesn't exist, creating it
		metric.Timestamps[id] = MetricTime{}
	}
	editMetric = metric.Timestamps[id]

	if timestampType == "S" {
		editMetric.Start = time.Unix(converted, 0)
	} else if timestampType == "E" {
		editMetric.End = time.Unix(converted, 0)
	}

	editMetric.Duration = editMetric.End.Sub(editMetric.Start).Seconds()
	editMetric.Comment = comment
	return nil, metric
}

func (s *ConductorServer) AddStructJob(clientType string, title string, timeout int, body []string, metrics []string) {
	// TODO Validate inputs
	var createdMetrics = make(map[string]string)
	for _, m := range metrics {
		var err, id, metric = s.addMetric(strings.SplitAfter(m, ":")[1])
		if err == nil {
			createdMetrics[id] = metric.Name
		}
	}
	var created = Job{uuid.New().String(), clientType, title, timeout, body, metrics}
	if _, ok := s.jobs[clientType]; !ok {
		s.jobs[clientType] = goqueue.New()
	}
	err := s.jobs[clientType].Push(created)
	Check(err)
}

func (s *ConductorServer) GrabJob(clientType string) (error, Job) {
	if _, ok := s.jobs[clientType]; ok {
		if s.jobs[clientType].Len() > 0 {
			return nil, s.jobs[clientType].Pop().(Job)
		}
	}
	return fmt.Errorf("no job found for client %s", clientType), Job{}
}

func (s *ConductorServer) GetMetricsGraph() chartjs.Chart {
	colors := []*types.RGBA{
		&types.RGBA{102, 194, 165, 220},
		&types.RGBA{250, 141, 98, 220},
		&types.RGBA{141, 159, 202, 220},
		&types.RGBA{230, 138, 195, 220},
	}
	chart := chartjs.Chart{Label: "Metrics chart"}
	var err error
	_, err = chart.AddXAxis(chartjs.Axis{Type: chartjs.Time, Position: chartjs.Bottom, ScaleLabel: &chartjs.ScaleLabel{FontSize: 22, LabelString: "Time", Display: chartjs.True}})
	Check(err)
	_, err = chart.AddYAxis(chartjs.Axis{Type: chartjs.Linear, Position: chartjs.Left,
		ScaleLabel: &chartjs.ScaleLabel{LabelString: "Metrics", Display: chartjs.True}})
	Check(err)

	for _, value := range s.GetMetrics() {
		var dataPoints chartjs.Dataset
		// Browse metric data points to create the dataset
		var points xy
		for _, valueTime := range value.Timestamps {
			points.x = append(points.x, valueTime.Duration)
			points.y = append(points.y, 1)
		}
		points = fixDataset(points)
		dataPoints = chartjs.Dataset{Data: points, BorderColor: colors[1], Label: value.Name, Fill: chartjs.False, PointRadius: 3, PointBorderWidth: 1, BackgroundColor: colors[0]}
		chart.AddDataset(dataPoints)
	}
	chart.Options.Responsive = chartjs.False
	return chart
}

func (s *ConductorServer) GetMetricsGraphPng() chart.Chart {
	series := make([]chart.Series, 0)

	for _, value := range s.GetMetrics() {
		// Browse metric data points to create the dataset
		var horizontal []time.Time
		var vertical []float64
		for _, valueTime := range value.Timestamps {
			horizontal = append(horizontal, valueTime.Start)
			vertical = append(vertical, 1)
			horizontal = append(horizontal, valueTime.End)
			vertical = append(vertical, 1)
		}
		series = append(series, chart.TimeSeries{
			XValues: horizontal,
			YValues: vertical,
			Name:    value.Name,
		})
	}

	return chart.Chart{
		Series: series,
		Title:  "Metrics evolution"}

}
