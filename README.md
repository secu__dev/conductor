# Overview

_Conductor_ is a framework designed to orchestrate and store distributed metrics.
It revolves around a server designed to store action requests / metrics updates, until clients execute them. 

Orchestrating test scenarii, measuring performances, and extracting metrics for each element processed, makes this a perfectly good integration tester, able to be added to a CI process, to have measurable & reproductible results.
Depending on your needs, as long as the API (described below) is followed, you'll be able to schedule, execute and check the results/metrics, on top of your own tests.

A scenario being a list of tasks added to a queue, multiple clients of the same type are able to go through the task queue in parallel, the queue is based on a FIFO (First In, First Out) behaviour, meaning parallel processing can be applied to data consumption.

There is no standard client developed at this time. Each client type should have a unique string identifier, to fetch tasks from the server's scenario queue. 

# API

#### Record a new client type (upon client start, to inform the server of its existence, and the required parameters expected)


/v1/register (POST) : id & syntax fields, in the body

 
#### Get the existing client types list

/v1/clients (GET) : json map <string,string>, key = client type identifier, value = expected parameters, comma-separated

_Exemple : { "HTTP_Client" : "url", "FTP_Client" : "url,login,password,file" }_



#### Get the task queue

 

/v1/jobs (GET) : json map<string,array>, key = client type identifier, value = struc list :

- id = job uuid,
- clientType = required client type to run the job,
- title = Job title,
- timeout = Job maximum running time (-1 = no timeout),
- body = String array matching client type's requested parameters (url,login,password,filefor instance for the FTP_Client)
- metrics = String array identifying metrics to fill in. Pattern = "\[SE\]:MetricName" ( _Exemple\: S\:FTP\_Query_ ) (Must start with either S: or E:, no SE: )


#### Add a task

 

/v1/jobs (POST) : json as defined in the task queue api (id, clientType, title, timeout, body, metrics)

 

#### Get the next task

 

/v1/jobs/{clientType} (GET) : Grabs the next task in the client type's queue. Reply = Json defined in the task queue (id, clientType, title, timeout, body, metrics)

Any job grabbed disappears from the server.

 

#### Get the list of all available metrics


/v1/metrics (GET) : Gets the list of all available metrics at the execution time. Json, map<string, struct> ; key = metric uuid, value = struct :

- name : Metric's name (without S: / E: header)
- times : map<string, struct> : key = object uuid, value = struct :
    - start : Date
    - stop : Date
    - duration_s : int (seconds between start & stop)
    - comment : string (Comment associated to the metric for this id)

 

#### Get all details on a single metric


/v1/metrics/{uuid} (GET) : Gets all the stored information on a metric (uuid might be replaced by the metric's name, will make it easier to query maybe).  Reply is a json struct for the metrics list just above.

 

#### Update a metric

 

/v1/metrics (POST) : Sends a metric update ; json body :

- name : metric's name, including start/end status (_S\:Download\_Start_, _E\:Download_Start_)
- id : object identifier (business identifier, used to follow an object through the metrics)
- value : timestamp (long)
- comment : Comment associated to that id's metric

 

#### Quick & dirty graph (based on durations alone)
 

/v1/graph (GET)


# Clients

Any and all clients matching the API can be developed. If you want to contribute a client to _Conductor_, and have it listed here, contact me!

# Run

This server is available as a Docker image.

To fetch the image, you only need to run this pull command :

        docker pull secudev/conductor-server

To run the server (default port is 8181, you can either change the exposed port with docker) :

        docker run -d --rm secudev/conductor-server

To change the port to 8585, use this command :

        docker run -d --rm -p 8585:8181 secudev/conductor-server

