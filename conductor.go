package main

import (
	"conductor/core"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/wcharczuk/go-chart"
	"log"
	"net/http"
	"net/http/httputil"
	"os"
	"strconv"
)

type ErrorResponse struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

var server = core.NewServer()

func main() {
	router := mux.NewRouter()

	router.HandleFunc("/v1/jobs", getJobs).Methods("GET")
	router.HandleFunc("/v1/jobs", addJob).Methods("POST")
	router.HandleFunc("/v1/jobs/{clientType}", grabJob).Methods("GET")

	router.HandleFunc("/v1/clients", getRegisteredClients).Methods("GET")
	router.HandleFunc("/v1/register", registerClient).Methods("POST")

	router.HandleFunc("/v1/metrics", getMetrics).Methods("GET")
	router.HandleFunc("/v1/metrics/{id}", getMetrics).Methods("GET")
	router.HandleFunc("/v1/metrics", addMetricInfo).Methods("POST")

	router.HandleFunc("/v1/graph", getMetricsGraph).Methods("GET")
	router.HandleFunc("/v1/graph2", getMetricsGraphPng).Methods("GET")

	argsWithoutProg := os.Args[1:]
	// Default port
	serverPort := ":8181"
	if len(argsWithoutProg) > 0 {
		if _, err := strconv.Atoi(argsWithoutProg[0]); err == nil {
			serverPort = ":" + argsWithoutProg[0]
		}
	}
	err := http.ListenAndServe(serverPort, router)
	if err != nil {
		fmt.Println(err.Error())
	}
}

func addMetricInfo(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.Header().Set("Content-Type", "application/json")

	var metricReport core.MetricReport
	err := json.NewDecoder(r.Body).Decode(&metricReport)
	if err == nil {

		// We've got all the values to work with now
		error, metric := server.AddMetricData(metricReport.Name, metricReport.ID, metricReport.Value, metricReport.Comment)
		if error == nil {
			json.NewEncoder(w).Encode(metric)
		} else {
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(ErrorResponse{http.StatusInternalServerError, "Unable to process given metric"})
		}
	} else {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(ErrorResponse{http.StatusInternalServerError, "Unable to read given input metric"})
	}
}

func registerClient(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.Header().Set("Content-Type", "application/json")

	var client core.ClientTypeRegistration
	err := json.NewDecoder(r.Body).Decode(&client)
	if err == nil {
		server.AddClientType(client.ID, client.Syntax)
		w.WriteHeader(http.StatusOK)
	} else {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(ErrorResponse{http.StatusInternalServerError, "Unable to read input data"})
	}
}

func getRegisteredClients(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(server.GetClientTypes())
}

func getMetrics(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	if _, ok := params["id"]; ok {
		// we have an ID, let's try & find it
		err, metric := server.GetMetric(params["id"])
		if err == nil {
			json.NewEncoder(w).Encode(metric)
		} else {
			w.WriteHeader(http.StatusNotFound)
			json.NewEncoder(w).Encode(ErrorResponse{http.StatusNotFound, "Metric Not Found"})
		}
	} else {
		// No ID, return the full list
		json.NewEncoder(w).Encode(server.GetMetrics())
	}
}

func getMetricsGraph(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.Header().Set("Content-Type", "text/html")

	chart := server.GetMetricsGraph()
	chart.SaveHTML(w, nil)
}

func getMetricsGraphPng(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.Header().Set("Content-Type", "image/png")

	graph := server.GetMetricsGraphPng()
	err := graph.Render(chart.PNG, w)
	if err != nil {
		log.Println(err)
	}
}

func addJob(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.Header().Set("Content-Type", "application/json")
	requestDump, e := httputil.DumpRequest(r, true)
	if e != nil {
		fmt.Println(e)
	}
	fmt.Println(string(requestDump))
	var job []core.Job
	err := json.NewDecoder(r.Body).Decode(&job)

	if err == nil {
		for _, j := range job {
			server.AddStructJob(j.ClientType,
				j.Title,
				j.Timeout,
				j.Body,
				j.Metrics)
		}
		w.WriteHeader(http.StatusOK)
	} else {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(ErrorResponse{http.StatusInternalServerError, "Unable to read input data"})
	}
}

func getJobs(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(server.GetJobs())
}

func grabJob(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.Header().Set("Content-Type", "application/json")

	params := mux.Vars(r)
	if _, ok := params["clientType"]; ok {
		err, nextJob := server.GrabJob(params["clientType"])
		if err == nil {
			json.NewEncoder(w).Encode(nextJob)
			return
		}
	}

	w.WriteHeader(http.StatusNotFound)
	json.NewEncoder(w).Encode(ErrorResponse{http.StatusNotFound, "Job matching requested type not found"})

}
