FROM golang:alpine as build

LABEL maintainer="SecuDev <remy.chaix@gmail.com>"

WORKDIR $GOPATH/src/conductor

COPY conductor.go .
RUN mkdir core
COPY core/work.go core/

RUN apk add --no-cache git mercurial \
 && go get -d -v ./... \
 && go install -v ./... \
 && apk del git mercurial

FROM alpine:latest

COPY --from=build /go/bin/conductor /usr/local/bin/conductor

ENTRYPOINT ["/usr/local/bin/conductor"]